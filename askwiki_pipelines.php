<?php
/**
 * Utilisations de pipelines par Askwiki
 *
 * @plugin     Askwiki
 * @copyright  2020
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Askwiki\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
