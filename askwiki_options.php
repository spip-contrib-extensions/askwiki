<?php
/**
 * Options au chargement du plugin Askwiki
 *
 * @plugin     Askwiki
 * @copyright  2020
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Askwiki\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
